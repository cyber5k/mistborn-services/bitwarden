#!/bin/bash

figlet "Mistborn: Bitwarden Credentials"

# generate bitwarden .env files
BITWARDEN_PROD_FILE="./.envs/.production/.bitwarden"
install -Dv /dev/null $BITWARDEN_PROD_FILE
echo "WEBSOCKET_ENABLED=true" >> $BITWARDEN_PROD_FILE
echo "SIGNUPS_ALLOWED=true" >> $BITWARDEN_PROD_FILE
